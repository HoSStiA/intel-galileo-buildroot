#!/bin/sh

set -u
set -e

PROGNAME=$(basename $0)

usage()
{
    echo "Create an SD card that boots on an Galileo Gen2 board."
    echo
    echo "Note: all data on the the card will be completely deleted!"
    echo "Use with care!"
    echo "Superuser permissions may be required to write to the device."
    echo
    echo "Usage: ${PROGNAME} <sd_block_device>"
    echo "Arguments:"
    echo "  <sd_block_device>     The device to be written to"
    echo
    echo "Example: ${PROGNAME} /dev/mmcblk0"
    echo
}

if [ $# -ne 1 ]; then
    usage
    exit 1
fi

if [ $(id -u) -ne 0 ]; then
    echo "${PROGNAME} must be run as root"
    exit 1
fi

DEV=${1}

# The partition name prefix depends on the device name:
# - /dev/sde -> /dev/sde1
# - /dev/mmcblk0 -> /dev/mmcblk0p1
if echo ${DEV}|grep -q mmcblk ; then
    PART="p"
else
    PART=""
fi

PART1=${DEV}${PART}1
PART2=${DEV}${PART}2

# Unmount the partitions if mounted
umount ${PART1} || true
umount ${PART2} || true

sync

# Partition the card.
# SD layout for Galileo Gen 2 boot:
# - DOS Partition Table
# - FAT Partiton (+128M)
# - Ext2 Partition, containing the root file system 
sfdisk --unit=M -D ${DEV} <<EOF
0,128,e,*
128,3968,83
EOF

sync

# Prepare a temp dir for mounting partitions
TMPDIR=$(mktemp -d)

# FAT partition: GRUB EFI Only
mkfs.vfat ${PART1}
mount ${PART1} ${TMPDIR}
cp -r output/images/efi-part/* ${TMPDIR}
sync
umount ${TMPDIR}

# ext4 partition: root filesystem
mkfs.ext2 ${PART2}
mount ${PART2} ${TMPDIR}
tar -C ${TMPDIR}/ -xf output/images/rootfs.tar
sync
umount ${TMPDIR}

# Cleanup
rmdir ${TMPDIR}
sync
echo Done
