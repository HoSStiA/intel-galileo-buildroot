#!/bin/sh
cp board/intel/galileo/grub.cfg ${BINARIES_DIR}/efi-part/EFI/BOOT/grub.cfg
cp board/intel/galileo/S10mdev ${TARGET_DIR}/etc/init.d/S10mdev
cp board/intel/galileo/force_hotplug ${TARGET_DIR}/sbin/force_hotplug
cp board/intel/galileo/mdev.conf ${TARGET_DIR}/etc/mdev.conf
